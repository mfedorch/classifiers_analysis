function BinDisp(varargin)
 % (TestImage, GrayTestImage, GTImage, BinarizedByWolf, BinarizedByLocO, 
% BinarizedBySau, BinarizedByO, BinarizedByAdaptT, BinarizedByNiblack, 
% BinarizedByKittler, BinarizedByBrensen, BinarizedByBradely, BinarizedByGatos)

f = 1; sp = 1;

for i = 1:length(varargin)
    figure(f); 
    subplot(3,1,sp); imshow(varargin{i});   title(sprintf('%s', inputname(i)));
    sp = sp + 1;
    
    if sp > 3
        sp = 1;
        f = f + 1;
    end;
end;
end