function Respond = ComparisingWithGT(ImageGT, varargin)

numvarargs = length(varargin); 

% if numvarargs > 10
%     error('myfuns:somefun2Alt:TooManyInputs', ...
%      'Possible parameters are: (image, [m n], k, offset, padding)');
% end
 
%For write down to .xlsx file obtained by this function data, use following
%code:
%filename = 'testdata.xlsx';
%sheet = 1;
%A = {'Recall', 'Precesion', 'FM', 'PSNR', 'NCC', 'NRM'};
%xlswrite(filename,A,sheet,'C1')

for i = 1:(numvarargs)
    ImageIn = varargin{i};
  
    BinName{i,1} = sprintf('%s', inputname(i+1));

    ImageGT = double(ImageGT);

    N_TP = sum(sum(~ImageIn & ~ImageGT));
    N_FP = sum(sum(~ImageIn &  ImageGT));
    N_FN = sum(sum( ImageIn & ~ImageGT));
    N_TN = sum(sum( ImageIn &  ImageGT));

    Recall(i,1) = N_TP/ (N_FN + N_TP);
    Precesion(i,1) = N_TP/ (N_FP + N_TP);
    FM(i,1) = (2 * Recall(i,1) * Precesion(i,1))/ (Recall(i,1) + Precesion(i,1)) * 100;

    MSE = sum( sum((ImageIn - ImageGT).^2))/ numel(ImageGT);
    C = 255;  % difference between foreground and background (possible to be 1)
    PSNR(i,1) = 10*log10(C^2/ MSE);
    
    CrossCorr(i,1) = corr2(ImageIn, ImageGT);
    
    NR_FN = N_FN./(N_FN + N_TP); %Negative Rate of False Negative
    NR_FP = N_FP./(N_FP + N_TN); %Negative Rate of False Positive
    NRM(i,1) = (NR_FN + NR_FP)/ 2; %Negative Rate Metric
    
end

abc(:,1) = (['A','B','C','D','E','F','G','H','I','J']); 
% abc2(:,1) = 1:10;

Comparising_with_GT_Evaluation_Metrics = table(Recall, Precesion, FM, PSNR, CrossCorr, NRM,...
     abc, 'RowNames', BinName);
 
% Comparising_with_GT_Evaluation_Metrics = sortrows(Comparising_with_GT_Evaluation_Metrics,'FM','descend')
Respond = Comparising_with_GT_Evaluation_Metrics;
end