function Respond = StatMeasure(varargin)
%SHORT DESCRIPTION HERE
%

numvarargs = length(varargin); 
ImageIn = varargin{1};

[N M] = size (ImageIn);
ProbArr = zeros(N, M, 'double');

for i = 1:numvarargs
    ProbArr = ProbArr + varargin{i};
end

ProbArr = ProbArr ./ numvarargs;
SumPr = sum(sum(ProbArr));

 for k = 1:numvarargs
     ImageIn = varargin{k};
     SumSk = sum(sum(ImageIn));
     SumPrSk = sum(sum(ProbArr .* ImageIn));
     StRecall(k,1) = SumPrSk/ SumSk;
     StPrecesion(k,1) = SumPrSk/ SumPr;
     StFM(k,1) = 100 * (2 * StRecall(k,1) * StPrecesion(k,1))/...
         (StRecall(k,1) + StPrecesion(k,1));
     
     StCrossCorr(k,1) = corr2(ImageIn, ProbArr);
     
     AltFN = sum(sum(ProbArr .* ~ImageIn));
     AltFP = sum(sum((1 - ProbArr) .* ImageIn));
     AltTN = sum(sum((1 - ProbArr) .* ~ImageIn));
     
     NR_FN = AltFN ./ (AltFN + SumPrSk); %Negative Rate of False Negative
	 NR_FP = AltFP ./(AltFP + AltTN); %Negative Rate of False Positive
	 StNRM(k,1) = (NR_FN + NR_FP)/ 2; %Negative Rate Metric
     
    MSE = sum( sum((ImageIn - ProbArr).^2))/ numel(ProbArr);
    C = 255;  % difference between foreground and background (possible to be 1)
    StPSNR(k,1) = 10*log10(C^2/ MSE);
     
     BinName{k,1} = sprintf('%s', inputname(k));
 end
 
abc(:,1) = (['A','B','C','D','E','F','G','H','I','J']);
% abc2(:,1) = 1:10;

Stat_Evaluation_Metrics = table(StRecall, StPrecesion, StFM, StPSNR, StCrossCorr, StNRM,...
     abc, 'RowNames', BinName);
% writetable(Stat_Evaluation_Metrics, 'testdata.xlsx', 'Sheet', 1, 'Range', 'I2', 'WriteRowNames', 1);
%  
% Stat_Evaluation_Metrics = sortrows(Stat_Evaluation_Metrics,'StFM','descend')
Respond = Stat_Evaluation_Metrics;
Stat_Evaluation_Metrics;
end